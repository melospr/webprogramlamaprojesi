﻿

namespace Entities.Messages
{
    public enum ErrorMessageCode
    {
        UserNameAlreadyExists = 101,
        EmailAlreadyExists = 102,
        UserIsNotActive = 151,
        UserNameOrPasswrong = 152,
        CheckYourEmail = 153,
        UserAlreadyActive = 154,
        ActivateIdDoesNotExist = 155,
        UserNotFound = 156,
        ProfileCouldNotUpdated = 157,
        UserCouldNotRemove = 158,
        UserCouldNotFind = 159,
        UserNotInserted = 160,
        UserCouldNotUpdated = 161,
        UsernameAlreadyExists = 162
    }
}
