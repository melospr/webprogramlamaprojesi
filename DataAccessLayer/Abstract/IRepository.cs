﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Abstract
{
    interface IRepository<T>
    {
        List<T> List();
        
        List<T> List(Expression<Func<T, bool>> where);
        
        T Find(Expression<Func<T, bool>> where);
        IQueryable<T> ListQueryable();

        int Save();
        
        int Update(T obj);
        
        int Delete(T obj);
        
        int Insert(T obj);

    }
}
