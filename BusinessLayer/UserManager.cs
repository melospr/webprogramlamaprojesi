﻿using Entities;
using Entities.ValueObjects;
using DataAccessLayer.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entities.Messages;
using Common.Helpers;
using BusinessLayer.Result;
using BusinessLayer.Abstract;

namespace BusinessLayer
{
    public class UserManager:ManagerBase<User>
    {
       
        public BusinessLayerResult<User> RegisterUser(RegisterViewModel data)
        {
            //Aktivasyon e-postası gönderimi.
            User user = Find(x=>x.Username == data.Username || x.Email == data.Email); //İstediğin bir kullanıcı bilgisini çekiyor.
            BusinessLayerResult<User> layerResult = new BusinessLayerResult<User>(); //Hataları BusinessLayerResult içerisinde topluyoruz.
            if (user != null)
            {
               if(user.Username == data.Username)
                {
                    layerResult.AddError(ErrorMessageCode.UserNameAlreadyExists, "Kullanıcı Adı Kayıtlı."); //Hata Listesine Kullanıcı Adı Aynı Hatasını ekledi 
                }
                if (user.Email == data.Email)
                {
                    layerResult.AddError(ErrorMessageCode.EmailAlreadyExists, "E-posta Adresi Kayıtlı.");//Hata listesine aynı Eposta olduğunu ekledi
                }

            }
            else
            {
                //Burada eğerki bir hata yoksa Data Base e değerleri döndürüyor insert kontrol ediliyor.
                int dbResult = base.Insert(new User()
                {
                    Username = data.Username,
                    Email = data.Email,
                    ProfileImageFileName = "user_boy.png",
                    Password = data.Password,
                    ActivateGuid = Guid.NewGuid(),
                    Isactive = false,
                    Isadmin = false
                });
                if (dbResult > 0)
                {
                   layerResult.Result = Find(x=> x.Email == data.Email  && x.Username == data.Username);
                    //layerResult.Result.ActivateGuid
                    string siteUri = ConfigHelper.Get<string>("SiteRootUrl");
                    string activateUri = $"{siteUri}/Home/ActivateUser/{layerResult.Result.ActivateGuid}";
                    string body = $"Merhaba {layerResult.Result.Username}; <br><br> Hesabınızı Aktifleştirmek için tıklayınız <a href='{activateUri}' target='_blank'> tıklayınız</a>.";
                    MailHelper.SendMail(body, layerResult.Result.Email,"Hesap Aktivasyonu");
                }
            }
            return layerResult;
        }

        public BusinessLayerResult<User> GetUserById(int id)
        {
            BusinessLayerResult<User> res = new BusinessLayerResult<User>();
            res.Result = Find(x => x.Id == id);
            if (res.Result == null)
            {
                res.AddError(ErrorMessageCode.UserNotFound,"Kullanıcı bulunamadı.");
            }
            return res;
        }

        public BusinessLayerResult<User> LoginUser(LoginViewModel data)
        {
            User user = Find(x => x.Username == data.Username && x.Password == data.Password); //İstediğin bir kullanıcı bilgisini çekiyor.
            BusinessLayerResult<User> layerResult = new BusinessLayerResult<User>(); //Hataları BusinessLayerResult içerisinde topluyoruz.
            layerResult.Result = user;
            if (user != null)
            {
                if (!user.Isactive)
                {
                    layerResult.AddError(ErrorMessageCode.UserIsNotActive, "Kullanıcı aktifleştirilmemiştir.");
                    layerResult.AddError(ErrorMessageCode.CheckYourEmail, "Lütfen hesabınızı e-postanızdan aktive ediniz.");
                }
            }
            else
            {
                layerResult.AddError(ErrorMessageCode.UserNameOrPasswrong, "Kullanıcı Adı ya da Şifre yanlış.");
            }
            return layerResult;
        }

        public BusinessLayerResult<User> UpdateProfile(User data)
        {
            User db_user =Find(x => x.Id != data.Id && (x.Username == data.Username || x.Email == data.Email));
            BusinessLayerResult<User> res = new BusinessLayerResult<User>();

            if (db_user != null && db_user.Id != data.Id)
            {
                if (db_user.Username == data.Username)
                {
                    res.AddError(ErrorMessageCode.UserNameAlreadyExists, "Kullanıcı adı kayıtlı.");
                }

                if (db_user.Email == data.Email)
                {
                    res.AddError(ErrorMessageCode.EmailAlreadyExists, "E-posta adresi kayıtlı.");
                }

                return res;
            }

            res.Result = Find(x => x.Id == data.Id); //DB den kullanıcı tekrar çekilir.
            res.Result.Email = data.Email;
            res.Result.Name = data.Name;
            res.Result.Surname = data.Surname;
            res.Result.Password = data.Password;
            res.Result.Username = data.Username;

            if (string.IsNullOrEmpty(data.ProfileImageFileName) == false)
            {
                res.Result.ProfileImageFileName = data.ProfileImageFileName;
            }

            if (base.Update(res.Result) == 0)
            {
                res.AddError(ErrorMessageCode.ProfileCouldNotUpdated, "Profil güncellenemedi.");
            }

            return res;
        }

        public BusinessLayerResult<User> RemoveUserById(int id)
        {
            BusinessLayerResult<User> res = new BusinessLayerResult<User>();
            User user = Find(x => x.Id == id);
           
            if (user != null)
            {
                if (Delete(user) == 0)
                {
                    res.AddError(ErrorMessageCode.UserCouldNotRemove, "Kullanıcı silinemedi.");
                    return res;
                }
            }
            else
            {
                res.AddError(ErrorMessageCode.UserCouldNotFind, "Kullanıcı bulunamadı.");
            }

            return res;
        }

        public BusinessLayerResult<User> ActivateUser(Guid activateId)
        {
            BusinessLayerResult<User> layerResult = new BusinessLayerResult<User>();
            layerResult.Result = Find(x => x.ActivateGuid == activateId);
            if (layerResult.Result != null)
            {
                if (layerResult.Result.Isactive)
                {
                    layerResult.AddError(ErrorMessageCode.UserAlreadyActive,"Kullanıcı Zaten Aktif Edilmiştir.");
                    return layerResult;
                }
                layerResult.Result.Isactive = true;
                Update(layerResult.Result);
            }
            else
            {
                layerResult.AddError(ErrorMessageCode.ActivateIdDoesNotExist, "Aktifleştirilecek Kullanıcı Bulunamadı.");
            }
            return layerResult;
        }
        public new BusinessLayerResult<User> Insert(User data)
        {
            //metod gizleme
            //Aktivasyon e-postası gönderimi.
            User user = Find(x => x.Username == data.Username || x.Email == data.Email); //İstediğin bir kullanıcı bilgisini çekiyor.
            BusinessLayerResult<User> layerResult = new BusinessLayerResult<User>(); //Hataları BusinessLayerResult içerisinde topluyoruz.
            layerResult.Result = data;
            if (user != null)
            {
                if (user.Username == data.Username)
                {
                    layerResult.AddError(ErrorMessageCode.UserNameAlreadyExists, "Kullanıcı Adı Kayıtlı."); //Hata Listesine Kullanıcı Adı Aynı Hatasını ekledi 
                }
                if (user.Email == data.Email)
                {
                    layerResult.AddError(ErrorMessageCode.EmailAlreadyExists, "E-posta Adresi Kayıtlı.");//Hata listesine aynı Eposta olduğunu ekledi
                }

            }
            else
            {
                layerResult.Result.ProfileImageFileName = "user_boy.png";
                layerResult.Result.ActivateGuid = Guid.NewGuid();
                //Burada eğerki bir hata yoksa Data Base e değerleri döndürüyor insert kontrol ediliyor.
                if(base.Insert(layerResult.Result)==0)
                {
                    layerResult.AddError(ErrorMessageCode.UserNotInserted, "Kullanıcı eklenemedi.");

                } 

            }
            return layerResult;

        }
        public new BusinessLayerResult<User> Update(User data)
        {
            User db_user = Find(x => x.Username == data.Username || x.Email == data.Email);
            BusinessLayerResult<User> res = new BusinessLayerResult<User>();
            res.Result = data;

            if (db_user != null && db_user.Id != data.Id)
            {
                if (db_user.Username == data.Username)
                {
                    res.AddError(ErrorMessageCode.UsernameAlreadyExists, "Kullanıcı adı kayıtlı.");
                }

                if (db_user.Email == data.Email)
                {
                    res.AddError(ErrorMessageCode.EmailAlreadyExists, "E-posta adresi kayıtlı.");
                }

                return res;
            }

            res.Result = Find(x => x.Id == data.Id);
            res.Result.Email = data.Email;
            res.Result.Name = data.Name;
            res.Result.Surname = data.Surname;
            res.Result.Password = data.Password;
            res.Result.Username = data.Username;
            res.Result.Isactive = data.Isactive;
            res.Result.Isadmin = data.Isadmin;

            if (base.Update(res.Result) == 0)
            {
                res.AddError(ErrorMessageCode.UserCouldNotUpdated, "Kullanıcı güncellenemedi.");
            }

            return res;
        }


    }
}
