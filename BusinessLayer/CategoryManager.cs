﻿using BusinessLayer.Abstract;
using DataAccessLayer.EntityFramework;
using Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer
{
    public class CategoryManager:ManagerBase<Category>
    {
        public override int Delete(Category category)
        {
            NoteManager notemanager = new NoteManager();
            LikedManager likedManager = new LikedManager();
            CommentManager commentManager = new CommentManager();
            //kattegory ile ilişkili notlar silinmeli
            foreach (Note note in category.Notes.ToList())
            {
                //notla ilişkili likeler silinmeli
                foreach (Liked like in note.Likes.ToList())
                {
                    likedManager.Delete(like);
                }
                //note ile ilgili commentlerin silinmesi
                foreach (Comment comment in note.Comments.ToList())
                {
                    commentManager.Delete(comment);
                }


                notemanager.Delete(note);
            }
            return base.Delete(category);
        }

    }
}
