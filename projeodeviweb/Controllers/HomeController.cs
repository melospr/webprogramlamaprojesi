﻿using BusinessLayer;
using BusinessLayer.Result;
using Entities;
using Entities.Messages;
using Entities.ValueObjects;
using projeodeviweb.Filters;
using projeodeviweb.Models;
using projeodeviweb.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;


namespace projeodeviweb.Controllers
{
    [Exc]
    public class HomeController : BaseController
    {
        private NoteManager noteManager = new NoteManager();
        private CategoryManager categoryManager = new CategoryManager();
        private UserManager userManager = new UserManager();
        public ActionResult Index()
        {
            return View(noteManager.ListQueryable().OrderByDescending(x=>x.ModifiedOn).ToList());  
        }

        public ActionResult MostLiked()
        {
            
            return View("Index", noteManager.ListQueryable().Where(x => x.IsDraft == false).OrderByDescending(x => x.LikeCount).ToList());
        }
        public ActionResult ByCategory(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            List<Note> notes = noteManager.ListQueryable().Where( x => x.IsDraft == false && x.CategoryId == id).OrderByDescending(x => x.ModifiedOn).ToList();
            return View("Index", notes);
        }
        [Auth]
        public ActionResult ShowProfile()
        {
            BusinessLayerResult<User> res = userManager.GetUserById(CurrentSession.user.Id);
            if (res.Errors.Count > 0)
            {
                ErrorViewModel notifyObj = new ErrorViewModel
                {
                    Title = "Hata Oluştu",
                    Items = res.Errors
                };
                TempData["errors"] = res.Errors;
                return View("Error", notifyObj);
            }
            return View(res.Result);
        }
        [Auth]
        public ActionResult EditProfile()
        {
            BusinessLayerResult<User> res = userManager.GetUserById(CurrentSession.user.Id);
            if (res.Errors.Count > 0)
            {
                ErrorViewModel notifyObj = new ErrorViewModel
                {
                    Title = "Hata Oluştu",
                    Items = res.Errors
                };
                TempData["errors"] = res.Errors;
                return View("Error", notifyObj);
            }
            return View(res.Result);
        }
        [Auth]
        [HttpPost]
        public ActionResult EditProfile(User model, HttpPostedFileBase ProfileImage)
        {
            ModelState.Remove("ModifiedUsername");
            if (ModelState.IsValid)
            {
              

                if (ProfileImage != null &&
                    (ProfileImage.ContentType == "image/jpeg" ||
                    ProfileImage.ContentType == "image/jpg" ||
                    ProfileImage.ContentType == "image/png"))
                {
                    string filename = $"user_{model.Id}.{ProfileImage.ContentType.Split('/')[1]}";

                    ProfileImage.SaveAs(Server.MapPath($"~/images/{filename}"));
                    model.ProfileImageFileName = filename;
                }

                BusinessLayerResult<User> res = userManager.UpdateProfile(model);

                if (res.Errors.Count > 0)
                {
                    ErrorViewModel errorNotifyObj = new ErrorViewModel()
                    {
                        Items = res.Errors,
                        Title = "Profil Güncellenemedi.",
                        RedirectingUrl = "/Home/EditProfile"
                    };

                    return View("Error", errorNotifyObj);
                }

                // Profil güncellendiği için session güncellendi.
                CurrentSession.Set<User>("login" , res.Result);
                return RedirectToAction("ShowProfile");
            }
            return View(model);
        }
        [Auth]
        public ActionResult DeleteProfile()
        {
            BusinessLayerResult<User> res = userManager.RemoveUserById(CurrentSession.user.Id);

            if (res.Errors.Count > 0)
            {
                ErrorViewModel errorNotifyObj = new ErrorViewModel()
                {
                    Items = res.Errors,
                    Title = "Profil Silinemedi.",
                    RedirectingUrl = "/Home/ShowProfile"
                };

                return View("Error", errorNotifyObj);
            }

            Session.Clear();

            return RedirectToAction("Index");
        }
        public ActionResult About()
        {
            return View();
        }
        public ActionResult Login()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Login(LoginViewModel model)
        {
            if (ModelState.IsValid)
            {
                
                BusinessLayerResult<User> res = userManager.LoginUser(model);
                if (res.Errors.Count > 0)
                {
                    if (res.Errors.Find(x => x.Code == ErrorMessageCode.UserIsNotActive) != null) //Localization yapılacağı yer.
                    {
                        ViewBag.SetLink = "E-posta Gönder.";
                    }
                    res.Errors.ForEach(x => ModelState.AddModelError("", x.Message));
                    
                    return View(model);
                }
                CurrentSession.Set<User>("login",res.Result); // Session bilgi tutma. 
                return RedirectToAction("Index"); // index actionuna yönlendirme.
            }
            return View();
        }
        public ActionResult Logout()
        {
            Session.Clear();
            return RedirectToAction("Index");
        }
        public ActionResult Register()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Register(RegisterViewModel model)
        {
            //Aktivasyon e-postası gönderimi.
            if (ModelState.IsValid)
            {
             
                BusinessLayerResult<User> res = userManager.RegisterUser(model);
                if (res.Errors.Count > 0)
                {
                    res.Errors.ForEach(x => ModelState.AddModelError("", x.Message));
                    return View(model);
                }
                OkViewModel notifyObj = new OkViewModel()
                {
                    Title = "Kayıt Başarılı",
                    RedirectingUrl = "/Home/Login",
                };
                notifyObj.Items.Add("Lütfen Eposta Adresinize gönderdiğimiz aktivasyon linkine tıklayarak hesabınızı aktive ediniz. NOT: aktive etmeden beğeni yapamıyor ve not yazamıyorsunuz...");
                return View("Ok",notifyObj);
            }
           
            return View(model);
        }
      
        public ActionResult ActivateUser(Guid id)
        {
           
            BusinessLayerResult<User> res = userManager.ActivateUser(id);
            if (res.Errors.Count > 0)
            {
                ErrorViewModel notifyObj = new ErrorViewModel
                {
                    Title = "Geçersiz İşlem",
                    Items = res.Errors
                };
                TempData["errors"] = res.Errors;
                return View("Error",notifyObj);
            }
            OkViewModel okNotifyObj = new OkViewModel
            {
                Title = "Hesap Aktifleştirildi.",
                RedirectingUrl="/Home/Login"
            };
            okNotifyObj.Items.Add("Hesabınız Aktifleştirildi. Artık not paylaşabilir ve beğenme, yorum yapabilirsiniz.");
            return View("Ok",okNotifyObj);
        }

        public ActionResult AccessDenied()
        {
            return View();
        }
        public ActionResult HasError()
        {
            return View();
        }
    }
}