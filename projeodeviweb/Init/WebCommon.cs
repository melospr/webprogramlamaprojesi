﻿using Common;
using Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace projeodeviweb.Init
{
    public class WebCommon : ICommon
    {
        public string GetUserName()
        {
            if (HttpContext.Current.Session["login"] != null)
            {
                User user = HttpContext.Current.Session["login"] as User;
                return user.Username; 
            }
            return "system";
        }
    }
}